#!/usr/bin/env python

from typing import *
from pprint import pprint
import csv, importlib, os, shutil, sys, time
import datetime as dt


def prep_dirs() -> None:
    target_dirs = []
    for file in os.listdir("submissions"):
        if file == "provided" or file[:-10] == "_grader.py":
            continue
        name = file.split("_")[0]
        canvas_id = file.split("_")[1]
        target_dir = os.path.join("submissions", canvas_id)
        target_dirs.append(target_dir)
        print("Preparing", target_dir, "for", file)
        target_file = "_".join(file.split("_")[3:])
        if len(target_file.split("-")) > 1:
            target_file = target_file.split("-")[0] + "." + target_file.split(".")[1]
        if not os.path.exists(target_dir):
            os.mkdir(target_dir)
        shutil.move(os.path.join("submissions", file), os.path.join(target_dir, target_file))
    if os.path.exists("provided"):
        print("Copying provided files...")
        for p in os.listdir("provided"):
            for target_dir in target_dirs:
                if not os.path.exists(os.path.join(target_dir, p)):
                    shutil.copy(os.path.join("provided", p), os.path.join(target_dir, p))


def main(argv: List[str]) -> None:
    prep_dirs()

if __name__=="__main__":
    main(sys.argv)
