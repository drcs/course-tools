#!/usr/bin/env python3

from pprint import pprint
import argparse
import csv
import sys

def make_argparser():
    parser = argparse.ArgumentParser(description='Generate class schedule.')
    parser.add_argument("-k", "--key", dest="key", required=True,
                        help="The key")
    parser.add_argument("-v", "--value", dest="value", required=True,
                        help="The value")
    parser.add_argument("-g", "--gradebook", dest="gb", required=True,
                        help="The gradebook CSV file")
    parser.add_argument("-s", "--slip-lines", dest="skip_lines", required=True,
                        help="The assignment to check")

    parser.add_argument("-o", "--output", dest="output", required=False,
                        help="File to write report to.")
    return parser

def main(argv):
    parser = make_argparser()
    args = parser.parse_args(argv[1:])
    key = args.key
    value = args.value
    gb = args.gb
    skip_lines = int(args.skip_lines)
    fout = open(args.output, 'w') if args.output else sys.stdout
    with open(args.gb) as fin, fout:
        reader = csv.DictReader(fin)
        # Remove junk rows Canvas adds after header
        for i in range(skip_lines):
            next(reader)
        missing = []
        for s in reader:
            if s[key] == value:
                missing.append((s["Student"], s["Section"].split("/")[3]))
        missing = sorted(missing, key=lambda t: t[1])
        for student, section in missing:
            print(student, section, file=fout)


if __name__=="__main__":
    main(sys.argv)
