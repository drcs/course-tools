#!/usr/bin/env python

from typing import *
from pprint import pprint
import argparse, csv, importlib, os, shutil, sys, time
import datetime as dt

def make_argparser():
    parser = argparse.ArgumentParser(description='Grade submissions in submissions directory')
    parser.add_argument("-g", "--grader", dest="grader", required=True,
                        help="Name of grader script")
    parser.add_argument("-r", "--regrades", dest="regrades", required=False,
                        help="File containing \\n-separated list of submissions to regrade")
    parser.add_argument("-s", "--start", dest="start", required=False,
                        help="Starting index (inclusive) of submissions to grade")
    parser.add_argument("-e", "--end", dest="end", required=False,
                        help="Ending index (exclusive) of submissions to grade")
    parser.add_argument("-x", "--extra-args", dest="extra", required=False,
                        help="Extra args to pass to the grader, comm-separated")
    return parser

def main(argv: List[str]) -> None:
    parser = make_argparser()
    args = parser.parse_args(argv[1:])
    extra_args = args.extra.split(",") if args.extra else []
    grader_module_name = args.grader.split(".")[0]
    # No idea why this is needed for the import to succeed.  Typical Python.
    sys.path.append(".")
    grader = importlib.import_module(grader_module_name)
    timestamp = str(dt.datetime.now().date()) + "T" + str(time.time())
    regrades = [id.strip() for id in open(args.regrades)] if args.regrades else []
    if regrades:
        print("Regrading", regrades)
    scoreswriter = csv.writer(open("regrades.csv", "wt", buffering=1)) if regrades\
                    else csv.writer(open("scores.csv", "wt", buffering=1))
    scoreswriter.writerow(["canvas_id", "score"])
    submissions = os.listdir("submissions")
    start = int(args.start) if args.start else 0
    end = int(args.end) if args.end else len(submissions)
    for submission in submissions[start:end]:
        if regrades and (submission not in regrades):
            continue
        if submission in regrades:
            grader.clean_submission_dir(os.path.join("submissions", submission))
        reportwriter = open(os.path.join("submissions",
                                         submission,f"{submission}-report.txt"),
                            "wt")
        print(f"Grading {submission}...")
        score, report = grader.grade(os.path.join("submissions", submission), *extra_args)
        scoreswriter.writerow([submission, str(score)])
        reportwriter.write(report)
        reportwriter.close()

if __name__=="__main__":
    main(sys.argv)
