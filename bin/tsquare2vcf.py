#!/usr/bin/env python

from typing import *
import argparse
import csv
import sys
import datetime as dt

def make_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description='Create VCF address book from TSquare CSV rosters')
    parser.add_argument("-r", "--rosters", dest="rosters", required=True,
                        help="Comma-separated list of roster CSV files (no spaces)")
    parser.add_argument("-g", "--group", dest="group", required=False,
                        help="Name of VCF address book group (CATEGORIES in VCF)")
    parser.add_argument("-o", "--output", dest="output", required=False,
                        help="Name of output VCF file (STDOUT if omitted)")
    return parser

def name2vcf(name: str) -> str:
    names = name.split(",")
    return names[0] + ";" + names[1] + ";"

def section2vcf(course: str, section: str) -> str:
    return f"{course}-{sec}"


def csvdict2vcard(record: Dict[str, str], course: str = None) -> str:
    result = "BEGIN:VCARD\nVERSION:3.0\n"
    result += f"N:{name2vcf(record['Name'])}\n"
    #result += f"FN:{record['Name']}\n"
    #result += f"GTID:{record['GTID']}\n"
    #result += f"GTLOGINID:{record['gtAccount']}\n"
    #result += f"ORG:{section2vcf(record['Section'])}\n"
    result += f"EMAIL:{record['Email Address']}\n"
    # if group:
    #     result += f"CATEGORIES:{group},{section2vcf(record['Section'])}\n"
    # else:
    result += f"NOTE:{course + '-' + record.get('Section', '')}\n"
    result += "END:VCARD"
    return result

def main(argv):
    parser = make_argparser()
    args = parser.parse_args(argv[1:])
    roster_fnames = args.rosters.split(",")
    fout = open(args.output, 'w') if args.output else sys.stdout
    for roster_fname in roster_fnames:
        course = roster_fname.split("-")[0]
        with open(roster_fname) as fin:
            reader = csv.DictReader(fin, delimiter=",")
            for record in reader:
                if record['Role'] == "Student":
                    print(csvdict2vcard(record, course), file=fout)

if __name__=="__main__":
    main(sys.argv)
