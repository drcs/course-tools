#!/usr/bin/env python

from typing import *
import argparse
import csv
import sys
import datetime as dt

def make_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description='Create VCF address book from Canvas CSV rosters')
    parser.add_argument("-r", "--rosters", dest="rosters", required=True,
                        help="Comma-separated list of roster CSV files (no spaces)")
    parser.add_argument("-g", "--group", dest="group", required=False,
                        help="Name of VCF address book group (CATEGORIES in VCF)")
    parser.add_argument("-o", "--output", dest="output", required=False,
                        help="Name of output VCF file (STDOUT if omitted)")
    return parser

def name2vcf(name: str) -> str:
    names = name.split(" ")
    # Special case hell
    if len(names) > 3:
        # There's a middle name and a Jr, II etc
        pass
    elif (len(names) > 2) and (names[-1].upper() != "JR"):
        # There's no middle name, but there's a Jr, etc
        pass
    elif len(names) > 2:
        # There's a middle name and no Jr, etc
        pass
    else:
        # There's only a first and last name
        pass

    return (names[-1] + ";" +
            names[0] + ";" +
            (names[1] if len(names) > 2 else ""))

def section2vcf(section: str) -> str:
    d1, dept, num, sec, d2 = section.split("/")
    return f"{dept}{num}-{sec}"


def csvdict2vcard(record: Dict[str, str], group: str = None) -> str:
    result = "BEGIN:VCARD\nVERSION:3.0\n"
    #result += f"N:{name2vcf(record['Name'])}\n"
    #result += f"FN:{record['Name']}\n"
    #result += f"GTID:{record['GTID']}\n"
    #result += f"GTLOGINID:{record['gtAccount']}\n"
    #result += f"ORG:{section2vcf(record['Section'])}\n"
    result += f"EMAIL:{record['Email']}\n"
    # if group:
    #     result += f"CATEGORIES:{group},{section2vcf(record['Section'])}\n"
    # else:
    result += f"NOTE:{section2vcf(record['Section'])}\n"
    result += "END:VCARD"
    return result

def main(argv):
    parser = make_argparser()
    args = parser.parse_args(argv[1:])
    roster_fnames = args.rosters.split(",")
    group = args.group
    fout = open(args.output, 'w') if args.output else sys.stdout
    for roster_fname in roster_fnames:
        with open(roster_fname) as fin:
            reader = csv.DictReader(fin, delimiter=",")
            for record in reader:
                if record['Role'] == "Student":
                    print(csvdict2vcard(record, group), file=fout)

if __name__=="__main__":
    main(sys.argv)
